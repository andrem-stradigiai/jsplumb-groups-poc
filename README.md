# stradigi-prototype

### Adding Groups

1. Create `group.component.ts`

These are the components we will use to render our Groups.  They extend `BaseNodeComponent` from the Toolkit's angular integration:

```
import {BaseNodeComponent} from "jsplumbtoolkit-angular";
import {Component, ViewChild} from "@angular/core";

@Component({ templateUrl:"templates/group.html" })
export class GroupComponent extends BaseNodeComponent {
    
    /*
        collapse/expand the group
    */
    toggleGroup(group:any) {
        this.surface.toggleGroup(group);
    }
 }
```

and are rendered via the templates `group-1.html` and `group-2.html`:

```
<div class="group-1">
    <div class="group-title">
        {{obj.title}}
        <button class="expand" (click)="toggleGroup(obj)"></button>
    </div>
    <div jtk-group-content="true"></div>
</div>
```

I've added some ports like the nodes have too.  Groups can be connected to nodes and vice versa, as well as having nodes internally.

2. Add GroupComponent and GroupComponent2 to the `entryComponents` and `declarations` in the module:

```

...

import { GroupComponent, Group2Component } from "./group.component"

@NgModule({
    imports:[ BrowserModule, jsPlumbToolkitModule, jsPlumbToolkitDragDropModule, ROUTING],
    declarations: [ AppComponent, DatasetComponent, FlowchartComponent, ControlsComponent, 
                    Block1Component, ButtonComponent, Block2Component, GroupComponent, Group2Component ],
    bootstrap:    [ AppComponent ],
    entryComponents: [ Block1Component, Block2Component, GroupComponent, Group2Component ],
    schemas:[ CUSTOM_ELEMENTS_SCHEMA ]
})
...

```


3. Map them in the view (in `flowchart.ts`):

```

...

view = {
    nodes:{
      "block-1":{
        component:Block1Component
      },
      "block-2":{
        component:Block2Component
      }
    },
    groups:{
      "group-1":{
        component:GroupComponent,
        layout:{
          type:"Absolute"             // groups of type 1 have an absolute layout
        }
      },
      "group-2":{
        component:Group2Component,
        layout:{
          type:"Circular"             // groups of type 2 have a circular layout
        }
      }
    },

...

```


4.  Test it out by adding a `groups` section to `data/flowchart-1.json` with a couple of groups, and then join the groups
and a couple of nodes inside group 1.  Note the nodes here in the json, how they have a `group` data member.

```
"nodes:":[

    ...

    {
        "id":"g1b1",
        "type":"block-1",
        "label":"G1 B1",
        "left":10,
        "top":10,
        "group":"group1"
      },

      {
        "id":"g1b2",
        "type":"block-2",
        "label":"G1 B2",
        "left":10,
        "top":100,
        "group":"group1"
      },

      {
        "id":"g2b1",
        "type":"block-1",
        "label":"G2 B1",
        "left":10,
        "top":10,
        "group":"group2"
      },

      {
        "id":"g2b2",
        "type":"block-2",
        "label":"G2 B2",
        "left":10,
        "top":100,
        "group":"group2"
      }

    ]
    "groups":[

      {
        "id":"group1",
        "type":"group-1",
        "label":"Group One",
        "left":650,
        "top":60
      },
      {
        "id":"group2",
        "type":"group-2",
        "label":"Group Two - circular",
        "left":650,
        "top":460
      }
    ],
    "edges": [
      { "source":"block1.out", "target":"block2.in"},
      { "source":"group1.out", "target":"group2.in"},
      { "source":"g1b1.out", "target":"g1b2.in"}
    ]


```        


5.  Adjust the z-index of jtk-endpoint and jtk-connector

The initial render of this drew everything fine except that the endpoints and connectors inside the groups were not visible, 
due to z-index issues, so I bumped the z-index of `.jtk-endpoint` and `.jtk-connector` to something suitable. When building 
apps with the Toolkit it pays to keep an eye on the z-index of the various bits and pieces.


6. Add groups to the palette of droppable things on the left.

This happens in a few stages.  First we add them to the list of draggables:

```
nodeTypes = [
    { type:"block-1", w:150, h:80, label:"TYPE 1" },
    { type:"block-2", w:150, h:80, label:"TYPE 2" },
    { type:"group-1", label:"GROUP TYPE 1", group:true },
    { type:"group-2", label:"GROUP TYPE 2", group:true }
  ]
```

Then we add an attribute to the renderer to call out which are groups:

```
<div *ngFor="let nodeType of nodeTypes" class="sidebar-item block {{nodeType.type}}" [attr.data-node-type]="nodeType.type" title="Drag to add new" [attr.jtk-width]="nodeType.w" [attr.jtk-height]="nodeType.h" [attr.data-group]="nodeType.group">
                <div class="left-block"></div>
                <div class="right-block">{{nodeType.label}}</div>
            </div>
```

We extract this attribute when the user starts to drag, as we'll want it on drop:

```
dataGenerator(el:Element) {
    return {
      type:el.getAttribute("data-node-type"),
      w:parseInt(el.getAttribute("jtk-width"), 10),
      h:parseInt(el.getAttribute("jtk-height"), 10),
      group:el.getAttribute('data-group')
    }
  }
```

And then finally on drop we figure out if we want to add a group or a node:

```
onCanvasDrop(surface:Surface, data:any, positionOnSurface:CanvasLocation) {
    data.left = positionOnSurface.left;
    data.top = positionOnSurface.top;
    if (data.group) {
      data.label = "New Group";
      surface.getToolkit().addGroup(data);
    } else {
      surface.getToolkit().addFactoryNode(data.type, data);
    }
    
  }
```

What is interesting for us here is that we've realised there is no `addFactoryGroup` method as the equivalent of
`addFactoryNode`, so we just added the group manually. The purpose of the `addFactoryNode` method is to use the `nodeFctory`
to get the details of a new node. It's defined in this demo in `app.component.ts`, and it pops up a dialog prompting the
user for a node name, also offering the ability to cancel. Our instinct was to add `addFactoryGroup` to the toolkit but our
implementation here would have duplicated almost all of the node factory, so we're going to look instead at combining the two.