import {Component, ElementRef, Input, ViewChild} from '@angular/core';

import {FlowchartComponent } from "./flowchart";
import {DatasetComponent } from "./dataset";
import { Dialogs, jsPlumbToolkit, jsPlumb, jsPlumbUtil  } from "jsplumbtoolkit";
import { jsPlumbService } from "jsplumbtoolkit-angular";

@Component({
    selector: 'jsplumb-demo',
    template:`
        <nav>
            <a routerLink="/home" style="cursor:pointer;" routerLinkActive="active">Flowchart</a>
            <a routerLink="/data" style="cursor:pointer;" routerLinkActive="active">Dataset</a>
        </nav>
        <router-outlet></router-outlet>       
        <div class="description">
          <p>
            Example application for Stradigi, using Angular 7.
          </p>
        </div>
    `
})
export class AppComponent {

  @ViewChild(FlowchartComponent) flowchart:FlowchartComponent;
  @ViewChild(DatasetComponent) dataset:DatasetComponent;

  toolkitId:string;
  toolkit:jsPlumbToolkit;

  constructor(private $jsplumb:jsPlumbService, private elementRef:ElementRef) {
    this.toolkitId = this.elementRef.nativeElement.getAttribute("toolkitId");
  }

  ngOnInit() {
    this.toolkit = this.$jsplumb.getToolkit(this.toolkitId, this.toolkitParams)
  }

  ngAfterViewInit() {
    this.toolkit.load({ url:"data/flowchart-1.json" });
  }

  toolkitParams = {
    nodeFactory:(type:string, data:any, callback:Function) => {
      Dialogs.show({
        id: "dlgText",
        title: "Enter " + type + " name:",
        onOK: (d:any) => {
          data.label = d.text;
          // if the user entered a name...
          if (data.label) {
            // and it was at least 2 chars
            if (data.label.length >= 2) {
              // set an id and continue.
              data.id = jsPlumbUtil.uuid();
              callback(data);
            }
            else
            // else advise the user.
              alert(type + " names must be at least 2 characters!");
          }
          // else...do not proceed.
        }
      });
    },
    beforeStartConnect:(node:any, edgeType:string) => {
      return { label:"..." };
    }
  }

}
