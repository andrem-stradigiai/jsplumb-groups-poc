import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { jsPlumbToolkitModule } from "jsplumbtoolkit-angular";
import { jsPlumbToolkitDragDropModule } from "jsplumbtoolkit-angular-drop";
import { Dialogs } from "jsplumbtoolkit";
import { ROUTING } from './app.routing';
import { DatasetComponent } from "./dataset";
import { ControlsComponent } from "./controls"

import { FlowchartComponent } from './flowchart';

import { ButtonComponent } from "./button.component";
import { Block1Component, Block2Component } from "./block1.component";
import { GroupComponent, Group2Component } from "./group.component"

@NgModule({
    imports:[ BrowserModule, jsPlumbToolkitModule, jsPlumbToolkitDragDropModule, ROUTING],
    declarations: [ AppComponent, DatasetComponent, FlowchartComponent, ControlsComponent, 
                    Block1Component, ButtonComponent, Block2Component, GroupComponent, Group2Component ],
    bootstrap:    [ AppComponent ],
    entryComponents: [ Block1Component, Block2Component, GroupComponent, Group2Component ],
    schemas:[ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {
    constructor() {
        // initialize dialogs
        Dialogs.initialize({
            selector: ".dlg"
        });
    }
}

