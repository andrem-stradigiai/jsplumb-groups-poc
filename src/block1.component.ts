import {BaseNodeComponent} from "jsplumbtoolkit-angular";
import {Component, ViewChild} from "@angular/core";
import { ButtonComponent } from "./button.component";

export class BlockComponent extends BaseNodeComponent {

  @ViewChild(ButtonComponent)
  button:ButtonComponent;

  event() {
    alert("the button was clicked on node " + this.getNode().id);
  }

  ngAfterViewInit() {
    this.button.listener = this;
  }

}

@Component({ templateUrl:"templates/block1.html" })
export class Block1Component extends BlockComponent { }

@Component({ templateUrl:"templates/block2.html" })
export class Block2Component extends BlockComponent { }
