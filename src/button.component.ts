import {Component} from "@angular/core";

import { BlockComponent } from "./block1.component";

@Component({
  templateUrl:"templates/button.html",
  selector:"DemoButton"
})
export class ButtonComponent {

  listener:BlockComponent;

  click() {
    this.listener && this.listener.event();
  }
}
