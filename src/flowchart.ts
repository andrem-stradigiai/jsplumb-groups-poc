import {Component, ViewChild} from '@angular/core';

import { Dialogs, jsPlumbToolkit, Surface, CanvasLocation } from "jsplumbtoolkit";

import { jsPlumbSurfaceComponent } from "jsplumbtoolkit-angular";

import { Block1Component, Block2Component } from "./block1.component";
import { GroupComponent, Group2Component } from "./group.component";


@Component({
  selector: 'jsplumb-flowchart',
  template: `
    <div class="sidebar node-palette" jsplumb-drag-drop selector=".block" 
         surfaceId="flowchartSurface" 
         [dataGenerator]="dataGenerator"
         [onCanvasDrop]="onCanvasDrop">
            <div *ngFor="let nodeType of nodeTypes" class="sidebar-item block {{nodeType.type}}" [attr.data-node-type]="nodeType.type" title="Drag to add new" [attr.jtk-width]="nodeType.w" [attr.jtk-height]="nodeType.h" [attr.data-group]="nodeType.group">
                <div class="left-block"></div>
                <div class="right-block">{{nodeType.label}}</div>
            </div>
    </div>
    <jsplumb-surface [surfaceId]="surfaceId" [toolkitId]="toolkitId" [view]="view" [renderParams]="renderParams"></jsplumb-surface>
    <jsplumb-miniview [surfaceId]="surfaceId"></jsplumb-miniview>
    <jsplumb-controls [surfaceId]="surfaceId"></jsplumb-controls>
`
})
export class FlowchartComponent {

  @ViewChild(jsPlumbSurfaceComponent) surfaceComponent:jsPlumbSurfaceComponent;

  toolkit:jsPlumbToolkit;
  surface:Surface;

  toolkitId:string;
  surfaceId:string;

  constructor() {
    this.toolkitId = "flowchart";
    this.surfaceId = "flowchartSurface";
  }

  getToolkit():jsPlumbToolkit {
    return this.toolkit;
  }

  toggleSelection(node:any) {
    this.toolkit.toggleSelection(node);
  }

  removeEdge(edge:any) {
    this.toolkit.removeEdge(edge);
  }

  nodeTypes = [
    { type:"block-1", w:150, h:80, label:"TYPE 1" },
    { type:"block-2", w:150, h:80, label:"TYPE 2" },
    { type:"group-1", label:"GROUP TYPE 1", group:true },
    { type:"group-2", label:"GROUP TYPE 2", group:true }
  ]

  onCanvasDrop(surface:Surface, data:any, positionOnSurface:CanvasLocation) {
    data.left = positionOnSurface.left;
    data.top = positionOnSurface.top;
    if (data.group) {
      data.label = "New Group";
      surface.getToolkit().addGroup(data);
    } else {
      surface.getToolkit().addFactoryNode(data.type, data);
    }
    
  }

  view = {
    nodes:{
      "block-1":{
        component:Block1Component
      },
      "block-2":{
        component:Block2Component
      }
    },
    groups:{
      "group-1":{
        component:GroupComponent,
        constrain:true
      },
      "group-2":{
        component:Group2Component,
        constrain:true
      }
    },
    edges: {
      "default": {
//        endpoint:[ "Dot", { radius:3 }],
        connector: ["Flowchart", { cornerRadius: 5 } ],
        paintStyle: { strokeWidth: 2, stroke: "rgb(132, 172, 179)", outlineWidth: 3, outlineStroke: "transparent" },	//	paint style for this edge type.
        hoverPaintStyle: { strokeWidth: 2, stroke: "rgb(67,67,67)" }, // hover paint style for this edge type.
        events: {
          "dblclick": (params:any) => {
            Dialogs.show({
              id: "dlgConfirm",
              data: {
                msg: "Delete Edge"
              },
              onOK: () => { this.removeEdge(params.edge); }
            });
          }
        }
      }
    },
    ports: {
      "darkblue": {
        endpoint: ["Dot", { radius:5 }],
        paintStyle: {fill: "#232AB3"}
      },
      "lightblue": {
        endpoint: ["Dot", { radius:5 }],
        paintStyle: {fill: "#73a2b3"}
      }
    }
  }

  renderParams = {
    layout:{
      type:"Spring"
    },
    consumeRightClick:false,
    dragOptions: {
      filter: ".jtk-draw-handle, .node-action, .node-action i"
    }
  }

  dataGenerator(el:Element) {
    return {
      type:el.getAttribute("data-node-type"),
      w:parseInt(el.getAttribute("jtk-width"), 10),
      h:parseInt(el.getAttribute("jtk-height"), 10),
      group:el.getAttribute('data-group')
    }
  }

  ngAfterViewInit() {
    this.surface = this.surfaceComponent.surface;
    this.toolkit = this.surface.getToolkit();
  }

  ngOnDestroy() {
    console.log("flowchart being destroyed");
  }

}
