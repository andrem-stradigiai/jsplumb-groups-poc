import {BaseNodeComponent} from "jsplumbtoolkit-angular";
import {Component, ViewChild} from "@angular/core";

@Component({ templateUrl:"templates/group-1.html" })
export class GroupComponent extends BaseNodeComponent {
    
    /*
        collapse/expand the group
    */
    toggleGroup(group:any) {
        this.surface.toggleGroup(group);
    }
 }

 @Component({ templateUrl:"templates/group-2.html" })
export class Group2Component extends BaseNodeComponent {
    
    /*
        collapse/expand the group
    */
    toggleGroup(group:any) {
        this.surface.toggleGroup(group);
    }
 }